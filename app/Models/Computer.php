<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Computer extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand', 'model', 'year', 'price', 'description', 'img', 'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
