<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComputerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'brand' => 'required|min:3|max:20',
            'model' => 'required|min:3|max:20',
            'year' => 'required|numeric|min:3|max:99999',
            'price' => 'required|numeric|min:1|max:999999',
            'description' => 'required|min:5|max:50000',
        ];
    }

    public function messages(){
        return [
            'brand.required' => 'La marca è obbligatoria',
            'brand.min' => 'La marca deve avere almeno 3 caratteri',
            'brand.max' => 'La marca deve avere al massimo 20 caratteri',

            'model.required' => 'Il modello è obbligatori',
            'model.min' => 'Il modello deve avere almeno 3 caratteri',
            'model.max' => 'Il modello deve avere al massimo 20 caratteri',

            'year.reuired' => 'L anno è obbligatorio',
            'year.numeric' => 'L anno deve essere necessariamente numerico',
            'year.min' => 'L anno deve avere almeno 3 caratteri',
            'year.max' => 'L anno deve avere al massimo 10 caratteri',

            'price.required' => 'Il prezzo è obbligatorio',
            'price.numeric' => 'Il prezzo deve essere necessariamente numerico',
            'price.min' => 'Il prezzo deve avere almeno 1 carattere',
            'price.max' => 'Il prezzo deve avere al massimo 999999 caratteri',

            'description.required' => 'La descrizione è obbligatoria',
            'description.min' => 'La descrizione deve avere almeno 5 caratteri',
            'description.max' => 'La descrizione deve avere al massimo 50000 caratteri',

        ];
    }
}
