<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Computer;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(){
        $phones = Phone::orderBy('created_at', 'DESC')->get();
        $computers = Computer::orderBy('created_at', 'DESC')->get();
        return view('admin.dashboard', compact('phones', 'computers'));
    }
}
