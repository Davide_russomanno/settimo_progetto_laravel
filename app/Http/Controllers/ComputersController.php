<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Computer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ComputerRequest;
use Illuminate\Support\Facades\Storage;

class ComputersController extends Controller
{
    public function create(){
        return view('computers.create');
    }

    public function store(ComputerRequest $request){

        if($request->file('img') == null){
            $img = 'default.png';
        } else {
            $img = $request->file('img')->store('public/computers');
        }

        Computer::create(
            [
                'brand' => $request->input('brand'),
                'model' => $request->input('model'),
                'year' => $request->input('year'),
                'price' => $request->input('price'),
                'description' => $request->input('description'),
                'img' => $img,
                'user_id' => Auth::user()->id,
            ]
        );
        return redirect()->route('phone.index')->with('message', "Grazie per aver inserito un nuovo computer!");
    }

    public function getComputersByBrand($brand){
        $computers = Computer::where('brand', $brand)->get();
        return view('computers.brand', compact('computers', 'brand'));
    }

    public function show(Computer $computer){
        return view('computers.show', compact('computer')); 
    }

    public function index(){
        $phones = Phone::orderBy('created_at', 'DESC')->get();
        $computers = Computer::orderBy('created_at', 'DESC')->get();
        return view('index', compact('phones', 'computers'));
    }

    public function edit(Computer $computer){
        return view('computers.edit', compact('computer'));
    }

    public function update(Computer $computer, Request $request){
        $computer->update(
            [
                'brand' => $request->input('brand'),
                'model' => $request->input('model'),
                'year' => $request->input('year'),
                'price' => $request->input('price'),
                'description' => $request->input('description'),
                'img' => $request->file('img') !== null ? $request->file('img')->store('public/computers') : $computer->img,
            ]
        );

        if($request->file('img') !== null){
            Storage::delete($computer->img);
        }
        return redirect()->route('phone.index')->with('message', "Computer $computer->model aggiornato correttamente");
    }

    public function delete(Computer $computer){
        $computer->delete();
        Storage::delete($computer->img);
        return redirect()->route('admin')->with('message', "Computer $computer->model cancellato correttamente");
    }
}
