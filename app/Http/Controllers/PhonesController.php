<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Computer;
use Illuminate\Http\Request;
use App\Http\Requests\PhoneRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PhonesController extends Controller
{
    public function create(){
        return view('phones.create');
    }

    public function store(PhoneRequest $request){

        if($request->file('img') == null){
            $img = 'default.png';
        } else {
            $img = $request->file('img')->store('public/phones');
        }

        Phone::create(
            [
                'brand' => $request->input('brand'),
                'model' => $request->input('model'),
                'year' => $request->input('year'),
                'price' => $request->input('price'),
                'description' => $request->input('description'),
                'img' => $img,
                'user_id' => Auth::user()->id,
            ]
        );
        return redirect()->route('phone.index')->with('message', "Grazie per aver inserito un nuovo smartphone!");
    }

    public function getPhonesByBrand($brand){
        $phones = Phone::where('brand', $brand)->get();
        return view('phones.brand', compact('phones', 'brand'));
    }

    public function show(Phone $phone){
        return view('phones.show', compact('phone')); 
    }

    public function index(){
        $phones = Phone::orderBy('created_at', 'DESC')->get();
        $computers = Computer::orderBy('created_at', 'DESC')->get();
        return view('index', compact('phones', 'computers'));
    }

    public function edit(Phone $phone){
        return view('phones.edit', compact('phone'));
    }

    public function update(Phone $phone, Request $request){
        $phone->update(
            [
                'brand' => $request->input('brand'),
                'model' => $request->input('model'),
                'year' => $request->input('year'),
                'price' => $request->input('price'),
                'description' => $request->input('description'),
                'img' => $request->file('img') !== null ? $request->file('img')->store('public/phones') : $phone->img
            ]
        );

        if($request->file('img') !== null){
            Storage::delete($phone->img);
        }
        return redirect()->route('phone.index')->with('message', "Smartphone $phone->model aggiornato correttamente");
    }

    public function delete(Phone $phone){
        $phone->delete();
        Storage::delete($phone->img);
        return redirect()->route('admin')->with('message', "Smartphone $phone->model cancellato correttamente");
    }
    
}
