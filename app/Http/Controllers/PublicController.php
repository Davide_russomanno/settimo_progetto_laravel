<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Computer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function home() {
        $phones = Phone::orderBy('created_at', 'desc')->get();
        $computers = Computer::orderBy('created_at', 'desc')->get();
        return view('welcome', ['phones' => $phones, 'computers' => $computers]);
    }

    public function profile(){
        return view('profile');
    }
}
