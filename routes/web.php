<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PhonesController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ComputersController;


Route::get('/', [PublicController::class, 'home'])->name('home');

Route::get('/phone/create', [PhonesController::class, 'create'])->name('phone.create');

Route::post('/phone/store', [PhonesController::class, 'store'])->name('phone.store');

Route::get('/phone/index', [PhonesController::class, 'index'])->name('phone.index');

Route::get('/phone/brand/{brand}', [PhonesController::class, 'getPhonesByBrand'])->name('phone.brand');

Route::get('/phones/show/{phone}', [PhonesController::class, 'show'])->name('phone.show');

Route::get('/phone/edit/{phone}', [PhonesController::class, 'edit'])->name('phone.edit');

Route::put('/phone/update/{phone}', [PhonesController::class, 'update'])->name('phone.update');

Route::delete('/phone/delete/{phone}', [PhonesController::class, 'delete'])->name('phone.delete');

Route::get('/computer/create', [ComputersController::class, 'create'])->name('computer.create');

Route::post('/computer/store', [ComputersController::class, 'store'])->name('computer.store');

Route::get('/computer/index', [ComputersController::class, 'index'])->name('phone.index');

Route::get('/computers/show/{computer}', [ComputersController::class, 'show'])->name('computer.show');

Route::get('/computer/edit/{computer}', [ComputersController::class, 'edit'])->name('computer.edit');

Route::put('/computer/update/{computer}', [ComputersController::class, 'update'])->name('computer.update');

Route::delete('/computer/delete/{computer}', [ComputersController::class, 'delete'])->name('computer.delete');

Route::get('/computer/brand/{brand}', [ComputersController::class, 'getComputersByBrand'])->name('computer.brand');

Route::get('/admin', [AdminController::class, 'dashboard'])->name('admin');

Route::get('/user/profile', [PublicController::class, 'profile'])->name('user.profile');









