<x-layout>

    @if (session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('message') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>Bentornato Admin</h1>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h2>Smartphone</h2>
                <a href="{{ route('phone.create') }}" class="btn btn-primary">Crea Smartphone</a>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12">
                <table class="table table-dark table-striped align-middle border">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modello</th>
                        <th scope="col">Anno</th>
                        <th scope="col">Prezzo</th>
                        <th scope="col" class="d-flex justify-content-center">Azioni</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($phones as $phone)
                            <tr>
                                <th scope="row">{{ $phone->id }}</th>
                                <td>{{ $phone->brand }}</td>
                                <td>{{ $phone->model }}</td>
                                <td>{{ $phone->year }}</td>
                                <td>€{{ $phone->price }}</td>
                                <td class="d-flex justify-content-evenly">
                                    <a href="{{ route('phone.edit', $phone) }}" class="btn btn-primary">Modifica</a>
                                    <form action="{{ route('phone.delete', $phone) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger">Cancella</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <h2>Computer</h2>
                <a href="{{ route('computer.create') }}" class="btn btn-primary">Crea Computer</a>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12">
                <table class="table table-dark table-striped align-middle border">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modello</th>
                        <th scope="col">Anno</th>
                        <th scope="col">Prezzo</th>
                        <th scope="col" class="d-flex justify-content-center">Azioni</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($computers as $computer)
                            <tr>
                            <th scope="row">{{ $computer->id }}</th>
                                <td>{{ $computer->brand }}</td>
                                <td>{{ $computer->model }}</td>
                                <td>{{ $computer->year }}</td>
                                <td>€{{ $computer->price }}</td>
                                <td class="d-flex justify-content-evenly">
                                    <a href="{{ route('phone.edit', $phone) }}" class="btn btn-primary">Modifica</a>
                                    <form action="{{ route('phone.delete', $phone) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger">Cancella</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>


</x-layout>