<div class="card" style="width: 18rem;">
  <img src="{{ Storage::url($phone->img) }}" class="card-img-top" alt="{{ $phone->model }}">
  <div class="card-body">
    <a href="{{ route('phone.brand', $phone->brand) }}" class="text-decoration-none">
      <h5 class="card-title">{{ $phone->brand }}</h5>
    </a>
    <p class="card-text stile-card">Modello: {{ $phone->model }}</p>
    <p class="card-text stile-card">Prezzo: €{{ $phone->price }}</p>
    <p class="card-text stile-card">Inserito da: {{ $phone->user->name }}</p>
    <a href="{{ route('phone.show', $phone) }}" class="btn btn-primary">Dettagli</a>
  </div>
</div>