<div class="card" style="width: 18rem;">
  <img src="{{ Storage::url($computer->img) }}" class="card-img-top" alt="{{ $computer->model }}">
  <div class="card-body">
    <a href="{{ route('computer.brand', $computer->brand) }}" class="text-decoration-none">
      <h5 class="card-title">{{ $computer->brand }}</h5>
    </a>
    <p class="card-text stile-card">Modello: {{ $computer->model }}</p>
    <p class="card-text stile-card">Prezzo: €{{ $computer->price }}</p>
    <p class="card-text stile-card">Inserito da: {{ $computer->user->name }}</p>
    <a href="{{ route('computer.show', $computer) }}" class="btn btn-primary">Dettagli</a>
  </div>
</div>