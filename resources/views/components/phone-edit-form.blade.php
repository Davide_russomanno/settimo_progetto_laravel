
<form method="POST" action="{{ route('phone.update', $phone) }}" enctype="multipart/form-data">
@method('PUT')
@csrf
<div class="mb-3">
    <label class="form-label">Marca dello smartphone</label>
    <input type="text" class="form-control @error('brand') is-invalid @enderror" name="brand" value="{{ $phone->brand }}">
    @error('brand')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>
<div class="mb-3">
    <label class="form-label">Modello dello smartphone</label>
    <input type="text" class="form-control @error('model') is-invalid @enderror" name="model" value="{{ $phone->model }}">
    @error('model')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>
<div class="mb-3">
    <label class="form-label">Anno dello smartphone</label>
    <input type="text" class="form-control @error('year') is-invalid @enderror" name="year" value="{{ $phone->year }}">
    @error('year')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>
<div class="mb-3">
    <label class="form-label">Prezzo dello smartphone</label>
    <input type="number" step="0.1" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $phone->price }}">
    @error('price')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>
<div class="mb-3">
    <label class="form-label">Immagine</label>
    <input type="file" class="form-control" name="img">
</div>
<div class="mb-3">
    <label class="form-label">Descrizione</label>
    <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="" cols="30" rows="10">{{ $phone->description }}</textarea>
    @error('description')
        <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
</div>
<button type="submit" class="btn btn-primary">Modifica</button>
</form>

