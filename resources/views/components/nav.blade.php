<nav class="navbar navbar-expand-md color-nav">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('home') }}">SEARCH TECH</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link @if(Route::currentRouteName() == 'home') color-link @endif fs-5" aria-current="page" href="{{ route('home') }}">Home</a>
        </li>
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'phone.create') color-link @endif fs-5" href="{{ route('phone.create') }}">Crea il tuo smatphone</a>
        </li>
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'computer.create') color-link @endif fs-5" href="{{ route('computer.create') }}">Crea il tuo computer</a>
        </li>
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'phone.index') color-link @endif fs-5" href="{{ route('phone.index') }}">Tutti i prodotti</a>
        </li>
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'admin') color-link @endif fs-5" href="{{ route('admin') }}">Admin</a>
        </li>
      </ul>
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        @if(Auth::user() == null)
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'login') color-link @endif fs-5" href="{{ route('login') }}">Accedi</a>
        </li>
        <li class="nav-item margin-link">
          <a class="nav-link @if(Route::currentRouteName() == 'register') color-link @endif fs-5" href="{{ route('register') }}">Registrati</a>
        </li>
        @else
          <li class="nav-item fs-4 color-form">
            <a href="{{ route('user.profile') }}" class="text-decoration-none">
            {{ Auth::user()->name }}
          </li>
          </a>
          <li class="nav-item fs-4 mx-5">
            <form action="{{ route('logout') }}" method="POST">
              @csrf
              <button class="btn btn-primary">Logout</button>
            </form>
          </li>
        @endif
      </ul>
    </div>
  </div>
</nav>