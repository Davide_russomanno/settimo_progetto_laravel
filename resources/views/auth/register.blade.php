<x-layout>

    <x-masthead></x-masthead>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="color-form">Registrati</h1>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 col-md-6">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label color-form">Nome</label>
                    <input type="text" name="name" class="form-control" id="exampleInputPassword1">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label color-form">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text color-form">We'll never share your email with anyone else.</div>
                    @error('email')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label color-form">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                    @error('password')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label color-form">Conferma Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1">
                    @error('password_confirmation')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>

    </div>

</x-layout>