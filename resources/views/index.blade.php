<x-layout>

    @if (session('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('message') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <div class="container-fluid sfondo-form">
        <div class="row">
            <div class="col-12 d-flex justify-content-center my-5">
                <h2>Smartphone inseriti dai nostri utenti</h2>
            </div>
        </div>
        <div class="row">      
            @if($phones->isNotEmpty())
                @foreach($phones as $phone)
                    <div class="col-12 col-md-3 my-2">
                        <x-phonecard
                        
                            :phone="$phone"

                        ></x-phonecard>
                    </div>
                @endforeach
            @else
                <div class="col-12 col-md-3 my-2">
                    <h2>Non ci sono smartphone</h2>        
                </div>
            @endif
        </div>
        <div class="row">  
            <div class="col-12 d-flex justify-content-center my-5">
                <h2>Computer inseriti dai nostri utenti</h2>
            </div>    
            @if($computers->isNotEmpty())
                @foreach($computers as $computer)
                    <div class="col-12 col-md-3 my-2">
                        <x-computercard
                        
                            :computer="$computer"

                        ></x-computercard>
                    </div>
                @endforeach
            @else
                <div class="col-12 col-md-3 my-2">
                    <h2>Non ci sono computer</h2>        
                </div>
            @endif
        </div>
    </div>

</x-layout>