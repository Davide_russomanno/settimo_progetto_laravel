<x-layout>

    <x-masthead></x-masthead>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>{{ $phone->model }}</h1>
            </div>
        </div>
	  <div class="row my-3">
            <div class="col-12 col-md-6">
                <img src="{{ Storage::url( $phone->img ) }}" alt="{{ $phone->model }}">
            </div>
		 <div class="col-12 col-md-6">
                <h2 class="stile-descrizione posizione-categoria">Marca</h2>
                <h4>{{ $phone->brand }}</h4>
                <h2 class="my-3 stile-descrizione">Dettagli</h2>
                <h5>{{ $phone->description }}</h5>
                <h2 class="my-3 stile-descrizione">Anno</h2>
                <h4>{{ $phone->year }}</h4>
                <h2 class="stile-descrizione">Prezzo</h2>
                <h4>€ {{ $phone->price }}</h4>
            </div>
        </div>
    </div>

</x-layout>