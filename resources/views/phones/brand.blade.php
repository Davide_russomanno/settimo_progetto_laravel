<x-layout>

    <x-masthead></x-masthead>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>Smartphone sviluppati da: {{ $brand }}</h2>
            </div>
        </div>
        <div class="row">
            @if($phones->isEmpty())
                <div class="col-12">
                    <h2>Non ci sono ancora brand che corrispondono alla tua ricerca</h2>
                </div>
            @else
                @foreach($phones as $phone)
                        <div class="col-12 col-md-3 my-2">
                            <x-phonecard
                            
                                :phone="$phone"

                            ></x-phonecard>
                        </div>
                @endforeach
            @endif
        </div>
    </div>

</x-layout>