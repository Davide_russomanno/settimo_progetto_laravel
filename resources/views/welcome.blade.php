<x-layout>

    <x-masthead></x-masthead>

    <div class="container-fluid sfondo-form">
        <div class="row">
            <div class="col-12 d-flex justify-content-center my-5">
                <h2>Smartphone inseriti dai nostri utenti</h2>
            </div>
        </div>
        <div class="row">      
            @if($phones->isNotEmpty())
                @foreach($phones as $phone)
                    <div class="col-12 col-md-3 my-2">
                        <x-phonecard
                        
                            :phone="$phone"

                        ></x-phonecard>
                    </div>
                @endforeach
            @else
                <div class="col-12 col-md-3 my-2">
                    <h2>Non ci sono smartphone</h2>        
                </div>
            @endif
        </div>
    </div>

</x-layout>