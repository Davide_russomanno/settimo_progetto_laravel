<x-layout>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>Bentornato {{ Auth::user()->name }}, questi sono gli elementi che hai creato!</h1>
            </div>
        </div>
        <div class="row my-5">
            @foreach(Auth::user()->phones as $phone)
                <div class="col-12 col-md-3 my-2">
                    <x-phonecard :phone="$phone" />
                </div>
            @endforeach
            @foreach(Auth::user()->computers as $computer)
                <div class="col-12 col-md-3 my-2">
                    <x-computercard :computer="$computer" />
                </div>
            @endforeach
        </div>
    </div>

</x-layout>