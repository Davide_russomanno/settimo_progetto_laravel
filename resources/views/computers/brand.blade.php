<x-layout>

    <x-masthead></x-masthead>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>Computer sviluppati da: {{ $brand }}</h2>
            </div>
        </div>
        <div class="row">
            @if($computers->isEmpty())
                <div class="col-12">
                    <h2>Non ci sono ancora brand che corrispondono alla tua ricerca</h2>
                </div>
            @else
                @foreach($computers as $computer)
                        <div class="col-12 col-md-3 my-2">
                            <x-computercard
                            
                                :computer="$computer"

                            ></x-computercard>
                        </div>
                @endforeach
            @endif
        </div>
    </div>

</x-layout>