<x-layout>

<x-masthead></x-masthead>

<div class="container-fluid sfondo-form">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Crea un nuovo computer</h1>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12 col-md-6">
                <x-formcomputer></x-formcomputer>
            </div>
        </div>
    </div>
 </div>

</x-layout>