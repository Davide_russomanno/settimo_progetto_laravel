<x-layout>

<x-masthead></x-masthead>

<div class="container-fluid sfondo-form">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>Modifica {{ $computer->model }}</h1>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-12 col-md-6">
                <x-computer-edit-form :computer="$computer" />
            </div>
        </div>
    </div>
 </div>

</x-layout>